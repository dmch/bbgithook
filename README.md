# AgileBench Service Broker for Bitbucket.org

Files:

+ broker.py - the actual AgileBenchBroker class
+ brokers.py - the fake bitbucket brokers.BaseBroker class
+ tests.py - a test which parses bitbucket's payload data and then repost to the URL specified in payload

...................